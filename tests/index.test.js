/* eslint no-magic-numbers: 0 */
import addAssertions from 'extend-tape';
import arrNotEqual from '..';
import tape from 'tape';

const test = addAssertions(tape, {arrNotEqual});

test(`------------ tape-arr-equals module test ------------`, (swear) => {
  swear.plan(1);
  swear.comment(`---- tape-arr-equals module test: arrNotEq ----`);
  swear.arrNotEqual([0, 1, 2, 4], [0, 1, 2, 3], `different is not equal`);
});
