# tape-arr-not-equal
[Tape](https://npmjs.com/package/tape) [extension](https://npmjs.com/package/extend-tape) that adds array inequality to tape.

## Install
``` bash
~ npm install tape-arr-not-equal

```

## Usage
Check out tests/index.text.js

``` javascript
import addAssertions from 'extend-tape';
import arrNotEqual from '..';
import tape from 'tape';

const test = addAssertions(tape, {arrNotEqual});

test(`------------ tape-arr-equals module test ------------`, (swear) => {
  swear.plan(1);
  swear.comment(`---- tape-arr-equals module test: arrNotEq ----`);
  swear.arrNotEqual([0, 1, 2, 4], [0, 1, 2, 3], `different is not equal`);
});

```
