{
  "ecmaFeatures": {
    "arrowFunctions": true,
    "blockBindings": true,
    "defaultParams": true,
    "destructuring": true,
    "modules": true
  },
  "env": {
    "es6": true,
    "node": true
  },
  "parser": "babel-eslint",
  "rules": {
    // 0 - Turn Rule Off
    // 1 - Turn Warning On For Rule
    // 2 - Turn Error On For Rule

    // Possible Errors //
    "comma-dangle": [2, "never"], // No comma's on last element
    "no-cond-assign": [2, "always"], // No single "=" in conditionals
    "no-console": 0, // No console.log in node
    "no-constant-condition": 1,  // No constant values in conditionals
    "no-control-regex": 1, // No special chars in regex
    "no-debugger": 0, // No using debugger statement
    "no-dupe-args": 2 , // No two arguements can have the same name
    "no-dupe-keys": 2, // No two object props/keys can have the same name
    "no-duplicate-case": 2 , // No two cases in switch can have the same name
    "no-empty-character-class": 2 , // No empty charactes in regex
    "no-empty": 2 , // No empty block statements
    "no-ex-assign": 2 , // No assignment of error refs
    "no-extra-boolean-cast": 2, // No double negation
    "no-extra-parens": [2, "functions"], // No extra parenthesis
    "no-extra-semi": 2, // No additional semi-colons (after 1)
    "no-func-assign": 2, // No reassignment of functions after declaration
    "no-inner-declarations": 2, // No function declarations allowed inner blocks
    //"no-invalid-regex": 2, // No bad regex
    "no-irregular-whitespace": 2, // No whitespace in bad places
    "no-negated-in-lhs": 2, // All "in" es6 for loops must be evaluated
    "no-obj-calls": 2, // Global JS objects aren't callable
    "no-regex-spaces": 2, // Use one actual space in regex then specify using {} syntax
    "no-sparse-arrays": 2 , // All slots in an array should be filled
    "no-unexpected-multiline": 2, // End all blocks with semi colons
    "no-unreachable": 2, // All code matters
    "use-isnan": 2, // Use is NaN to compare with NaN
    "valid-jsdoc": 2, // Syntax check on JSDOC
    "valid-typeof": 2, // When using typeof in comparison make sure string is valid
    // Best Practices //
    "accessor-pairs": 0, //
    "block-scoped-var": 2, // Don't use variables outside scope
    "complexity": [1, 3], // Level 3 complexity for cyclomacy
    "consistent-return": 2, // All returns from func should be same type
    "curly": 2, // All blocks require curly braces
    "default-case": 2, // Switches need default case
    "dot-location": [2, "property"], // Multiline object declarations put dot on property
    "dot-notation": 2, // Use foo.bar vs foo["bar"]
    "eqeqeq": 2, // Triple equal for comparison
    "guard-for-in": 1, // Warn if for ... in does not filter with if block
    "no-alert": 1, // Warn when using alert, confirm, prompt, etc
    "no-caller": 2, // No use of caller/callee
    "no-case-declarations": 2, // Don't create variables in cases
    "no-div-regex": 1, // Regex that looks like division warn
    "no-else-return": 2, // If block contains return, no else is necessary
    "no-empty-label": 2, // Don't use labels
    "no-empty-pattern": 2, // When destructuring, avoid creating empty patterns
    "no-eq-null": 2, // Don't test null equality
    "no-eval": 2, // No use of eval function
    "no-extend-native": 2, // No extension, overwrite of native objects
    "no-extra-bind": 2, // Don't use bind(), unless necessary
    "no-fallthrough": 2, // Use break after every switch case
    "no-floating-decimal": 2, // Pre or Post pend every decimal with a 0
    "no-implicit-coercion": 2, // Use more concise sytax for converting for comparison
    "no-implied-eval": 2, // Explicitely specify function, no eval()
    "no-invalid-this": 0, // No 'this' outside of object or class
    "no-iterator": 2, // Don't use the iterator property
    "no-labels": 2, // Don't use labled statemetns
    "no-lone-blocks": 1, // Single line blocks are useless, warn against usage
    "no-loop-func": 2, // Functions create closures, this is a no-no in loops
    "no-magic-numbers": 1, // Repeated values should be abstracted into contants
    "no-multi-spaces": 2, // We want consistent spaces,
    "no-multi-str": 2, // If not using es6 string templates, multiline is a no-no
    "no-native-reassign": 2, // Don't overwrite native objects
    "no-new-func": 2, // Let's not use the Function constructor to make functions
    "no-new-wrappers": 2, // Don't create primitive using native wrapper (makes them objects)
    "no-new": 2, // Always use 'new' operator
    "no-octal-escape": 2, // Octal escapes are deprcated, use Unicode
    "no-octal": 1, // Warn against es5 octal literals (beginning with 0)
    "no-param-reassign": 2, // Don't modify passed params
    "no-process-env": 2, // Set environment globally or in separate file
    "no-proto": 2, // Don't overwrite built in object proto property
    "no-redeclare": 2, // Declare once, never again (in same file)
    "no-return-assign": 2, // No assignments in return statements
    "no-script-url": 2, // javascript: is a form of eval, so no eval
    "no-self-compare": 2, // Don't compare variable to itself
    "no-sequences": 1, // No multiple expressions where one is needed
    "no-throw-literal": 2, // Throw a class or variable, no literals
    "no-unused-expressions": 2, // Code is precious, don't leave any hanging around
    "no-useless-call": 2, // .apply() and .call() are slow, only use when necessary
    "no-useless-concat": 2, // Never concat two strings!
    "no-void": 2, // Void returns undefied, cut out the middle man and use directly
    "no-warning-comments": 0, // We want todo, fix etc comments
    "no-with": 2, // No third wheels, (With adds an objects memebrs to scope)
    "radix": 2, // Always specify radix when using ParseInt()
    "vars-on-top": 1, // Though hoisting exists, try and declare them at the top of scope
    "wrap-iife": 2, // Wrap self invoking fuctions in parens
    "yoda": 2, // Literals on right of comparison
    // Variables //
    "init-declarations": 0, // Initialize variables at declaration or don't, eh
    "no-catch-shadow": 2, // No overwriting of error variable in catch
    "no-delete-var": 2, // Variables can't be deleted, so don't try it
    "no-label-var": 2, // No key/property can have the same name as a variable
    "no-shadow-restricted-names": 2, // No naming of variables after keywords
    "no-shadow": [2, {"builtinGlobals": true, "allow": ["tape", "t", "swear"]}], // No reuse of global variables inside blocks (non-param)
    "no-undef-init": 2, // Don't declare variable with value undefined
    "no-undef": 2, // Don't used variable that's not defined
    "no-undefined": 2, // Don't overwrite undefined
    "no-unused-vars": 2, // All vars matter
    "no-use-before-define": 2, // Declare before use (hoisting? who cares!)
    // Stylistic //
    "array-bracket-spacing": 2, // No spaces in array declaration
    "block-spacing": 2, // Spaces after curly braced one liners
    "brace-style": [2, "1tbs", {"allowSingleLine": true}], // Opening curly brace with statement, else on same line as closing
    "camelcase": [2, {"properties": "never"}], // Use camelcase for variables only not properties
    "comma-spacing": [2, {"before": false, "after": true}], // Commas should be attached to preceding var/param
    "comma-style": [2, "last"], // Comma, separated lists attach comma to preceding element
    "computed-property-spacing": [2, "never"], // No spaces inside property accessors
    "consistent-this": 0, // With es6 arrows this shouldn't be used as frequently
    "eol-last": 2, // Require new line as last line in file
    "func-names": 2, // Function expressions should be named
    "func-style": [1, "declaration", {"allowArrowFunctions": true}], // Prefer functions to be declared first
    "id-length":[2, {"min": 3, "properties": "never", "exceptions": ["f", "i", "x", "y"]}], // Keep property names at sane length
    "id-match": 0, // Name things whatevs
    "indent": [2, 2], // Two spaces indentation!
    "jsx-quotes": [2, "prefer-single"],
    "key-spacing": [2, {"beforeColon": false, "afterColon": true}], // Space on key for object
    "linebreak-style": [2, "unix"], // Unix style line breaks
    "lines-around-comment": 0, // Comments next to code, ehhh whatevs
    "max-depth": [1, 4], // Let's not get toooooo deep
    "max-len": [1, 80, 4], // Let's aim for 80 character lines but no biggie
    "max-nested-callbacks": [1, 3], // We shouldn't need call backs when having Promises but "jic"
    "max-params": [1, 4], // Any more than 3 params and we should just use "rest" or "spread"
    "max-statements": 0, // Who cares how many statments are in a function
    "new-cap": 2, // Constructors should start with capitals
    "new-parens": 2, // Constructors should be declared with parens
    "newline-after-var": [2, "always"], // Separate declarations from rest of code
    "no-array-constructor": 2, // Use braces not Array constructor
    "no-bitwise": 2, // && or ||
    "no-continue": 1,// Try not to use continues
    "no-inline-comments": 2, // These are sloppy, docs should provide all explanation
    "no-lonely-if": 2, // Instead of if inside else, use else if
    "no-mixed-spaces-and-tabs": 2, // You can get with this or you can get with that
    "no-multiple-empty-lines": [2, {"max": 3}], // Only 3 blank lines
    "no-negated-condition": 2, // Don't invert, just switch effect clauses
    "no-nested-ternary": 2, // ARE YOU CRAY CRAY FOR THE REAL REAL NOT FOR THE PLAY PLAY??
    "no-new-object": 2, // Don't invoke Object constructor
    "no-plusplus": 0, // I quite like ++ and --
    "no-restricted-syntax": 0, // Nah
    "no-spaced-func": 2, // Attach parens to function call
    "no-ternary": 0, // Ternary is good
    "no-trailing-spaces": 2,
    "no-underscore-dangle": 2, // Underscore before not after
    "no-unneeded-ternary": [2, { "defaultAssignment": false }], // Don't use ternary to assing boolean
    "object-curly-spacing": [2, "never"], // No spaces on objects
    "one-var": [2, "always"], // Comma delimit variables
    "operator-assignment": [2, "always"], // Shorten opeartor assignments
    "operator-linebreak":[2, "after"] , // Operator before next element
    "padded-blocks": [2, "never"], // Concise and compact blocks
    "quote-props": [2, "consistent"], // Don't quote object properties
    "quotes": [2, "backtick"], // Backticks only for strings
    "require-jsdoc": [2, {"require":{"FunctionDeclaration": true, "MethodDefinition": true, "ClassDeclaration": false}}], // JSDOC is good
    "semi-spacing": 2, // func; not func ;
    "semi": 2, // Ya need semi colons
    "sort-vars": 0, // Don't alphabetize  vars
    "space-after-keywords": 2, // Space after keywords
    "space-before-blocks": 2, // Yes
    "space-before-function-paren": 2, // Yup
    "space-before-keywords": [2, "always"], // Yes
    "space-in-parens": 2, // No space in parens
    "space-infix-ops": 2, // space before and after operators
    "space-return-throw-case": 2, // Yup
    "space-unary-ops": 0, // We shouldn't be using theme
    "spaced-comment": 2, // Yup
    "wrap-regex": 2, // Wrap regex in parens
    // ES6 //
    "arrow-body-style": [2, "always"], // Wrap arrow funcs with braces, unless one-liners
    "arrow-parens": [2, "always"], // Always wrap params for arrow func in parens
    "arrow-spacing": [2, {"before": true, "after": true}], // Space before and after fat arrow
    "constructor-super": 2, // Derived classes must use super, non derived must not
    "generator-star-spacing": 2, // Consistent use of * for generators
    "no-arrow-condition": 2, // Don't confure fat arrow for other operator
    "no-class-assign": 2, // Don't overwrite class
    "no-const-assign": 2, // Can't assign value to const after declaration
    "no-dupe-class-members": 2, // No class props or funcs can have same name
    "no-this-before-super": 2, // Handle derived class before referencing this scope
    "no-var": 2, // Use const or let instead of var
    "object-shorthand": 2, // Must use object shorthand
    "prefer-arrow-callback": 1, // Use fat arrow over function call
    "prefer-const": 2, // If value is declared and never changed, use const yea?
    "prefer-reflect": 0, // Don't worry about reflect API
    "prefer-spread": 2, // Use ... instead of .apply()
    "prefer-template": 2, // Template string instead of concat
    "require-yield": 1, // Generators need a yield
  }
}
